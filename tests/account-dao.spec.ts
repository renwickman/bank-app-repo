import { AccountDAO } from "../src/DAOs/account-dao";
import { Account, Client } from "../src/entities";
import { client } from "../src/connection";
import { AccountDaoPostgres } from "../src/DAOs/account-dao-postgres";

const accountDAO:AccountDAO = new AccountDaoPostgres();
const testClient:Client = new Client(0, 'Oswald', 'Cobblepot');
const testAccount:Account = new Account(0, 223344556, 500, "Savings", testClient.clientId + 1);

test("Create an account", async () =>{
    const result = await accountDAO.createAccount(testAccount);
    expect(result.accountId).not.toBe(0);
    console.log(result);
});

test("Get account by Id", async ()=>{
    let account:Account = new Account(0, 223344556, 500, "Savings", testClient.clientId + 1);
    account = await accountDAO.createAccount(account);

    let retrievedAccount:Account = await accountDAO.getAccountById(account.accountId);
    //console.log(typeof retrievedAccount.acctNumber);
    //expect(retrievedAccount.accountId).toBe(account.accountId);
    expect(retrievedAccount.acctNumber).toBe(account.acctNumber);
});

test("Get all accounts", async ()=>{
    let account1:Account = new Account(0, 223344556, 500, "Checking", testClient.clientId + 1);
    let account2:Account = new Account(0, 223344556, 500, "Checking", testClient.clientId + 1);
    let account3:Account = new Account(0, 223344556, 500, "Checking", testClient.clientId + 1);
    let account4:Account = new Account(0, 223344556, 500, "Checking", testClient.clientId + 1);
    await accountDAO.createAccount(account1);
    await accountDAO.createAccount(account2);
    await accountDAO.createAccount(account3);
    await accountDAO.createAccount(account4);

    const accounts:Account[] = await accountDAO.getAllAccounts();

    expect(accounts.length).toBeGreaterThanOrEqual(4);
});

test("Update account", async() => {
    let targetedAccount:Account = new Account(0, 223344556, 500, "Checking", testClient.clientId + 1);
    targetedAccount = await accountDAO.createAccount(targetedAccount);
    targetedAccount.type = 'Savings';

    targetedAccount = await accountDAO.updateAccount(targetedAccount);


    expect(targetedAccount.type).toBe("Savings");
});

test("Delete account", async () =>{
    let targetedAccount:Account = new Account(0, 223344556, 500, "Savings", testClient.clientId + 1);
    targetedAccount = await accountDAO.createAccount(targetedAccount);
    console.log(targetedAccount);

    const result:boolean = await accountDAO.deleteAccountById(targetedAccount.accountId);
    expect(result).toBeTruthy();
});

afterAll(async()=>{
    client.end()
});