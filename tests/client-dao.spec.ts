import { ClientDAO } from "../src/DAOs/client-dao";
import { Client } from "../src/entities";
import { client } from "../src/connection";
import { ClientDaoPostgres } from "../src/DAOs/client-dao-postgres"

const clientDAO:ClientDAO = new ClientDaoPostgres();
const testClient:Client = new Client(0, 'Edward', 'Nigma');

test("Create a client", async () =>{
    const result:Client = await clientDAO.createClient(testClient);
    console.log(result);
    expect(result.clientId).not.toBe(0); // An entity saved should have a non-zero id
});

test("Get client by Id", async ()=>{
    let myClient:Client = new Client(0, 'Oswald', 'Cobblepot');
    myClient = await clientDAO.createClient(myClient);

    let returnedClient:Client = await clientDAO.getClientById(myClient.clientId);

    expect(returnedClient.fname).toBe(myClient.fname);
});

test("Get all clients", async ()=>{
    let client1:Client = new Client(0, 'Selena', 'Kyle');
    let client2:Client = new Client(0, 'Harvey', 'Dent');
    let client3:Client = new Client(0, 'Tommy', 'Elliot');
    let client4:Client = new Client(0, 'Jonathan', 'Crane');
    await clientDAO.createClient(client1);
    await clientDAO.createClient(client2);
    await clientDAO.createClient(client3);
    await clientDAO.createClient(client4);

    const clients:Client[] = await clientDAO.getAllClients();

    expect(clients.length).toBeGreaterThanOrEqual(4);
});

test("Update client", async() => {
    let targetedClient:Client = new Client(0, 'Max', 'Holloway');
    targetedClient = await clientDAO.createClient(targetedClient);
    targetedClient.lname = 'Payne';

    targetedClient = await clientDAO.updateClient(targetedClient);

    expect(targetedClient.lname).toBe("Payne");
});


test("Delete client by id", async() => {
    let targetedClient:Client = new Client(0, 'Max', 'Holloway');
    targetedClient = await clientDAO.createClient(targetedClient);

    const result:boolean = await clientDAO.deleteClientById(targetedClient.clientId);
    expect(result).toBeTruthy();
});


afterAll(async()=>{
    client.end()
});