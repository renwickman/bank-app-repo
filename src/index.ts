import express from 'express'
//import { OverdrawnError } from '../bank-app-repo/src/errors';
import { Client, Account } from './entities'
import { MissingResourceError, OverdrawnError } from './errors';

import AccountService from './services/account-service';
import { AccountServiceImpl } from './services/account-service-impl';
import ClientService from './services/client-service';
import { ClientServiceImpl } from './services/client-service-impl';

const app = express();
app.use(express.json());

const clientService:ClientService = new ClientServiceImpl();
const accountService:AccountService = new AccountServiceImpl();

// POST /clients => Creates a new client
// 	return a 201 status code
app.post("/clients", async (req, res) =>{
    let client:Client = req.body;
    client = await clientService.registerClient(client);
    res.send(client);
    res.status(201);
});

// GET /clients => gets all clients
// 	return 200
app.get("/clients", async (req, res) => {
    const clients:Client[] = await clientService.retrieveAllClients();
    res.send(clients);
    res.status(200);
});

// GET /clients/10 => get client with id of 10
// 	return 404 if no such client exist
app.get("/clients/:id", async (req, res) => {
    try {
        const clientId = Number(req.params.id);
        const client = await clientService.retrieveClientById(clientId);
        res.send(client);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// PUT /clients/12 => updates client with id of 12	
// 	return 404 if no such client exist
app.put("/clients/:id", async (req, res) => {
    try{
        const clientId = Number(req.params.id);
        const newClient:Client = await clientService.retrieveClientById(clientId)
        const client = await clientService.updateClient(newClient);
        res.send(client);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});


// DELETE /clients/15 => deletes client with the id of 15
// 	return 404 if no such client exist
// 	return 205 if success
app.delete("/clients/:id", async (req, res) => {
    try {
        const clientId = Number(req.params.id);
        await clientService.removeClientById(clientId);
        res.status(205);
        res.send(`${clientId} has been deleted.`);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});


// POST /clients/5/accounts =>creates a new account for client with the id of 5
// 	return a 201 status code
app.post("/clients/:id/accounts", async (req, res) =>{
    const clientId = Number(req.params.id);
    const acct:Account = req.body;
    const newAcct:Account = await accountService.registerAccount(acct, clientId);
    res.send(newAcct);
    res.status(201);
});



// GET /clients/7/accounts => get all accounts for client 7
// 	return 404 if no client exists
app.get("/clients/:id/accounts", async (req, res)=>{
    try {
        let theCltAccts = [];
        const currentId = Number(req.params.id);
        let cltAccounts:Account[] = await accountService.retrieveAllAccounts();
        for (let i = 0; i < cltAccounts.length; i++){
            if (cltAccounts[i].clientId === currentId){
                theCltAccts.push(cltAccounts[i]);
            }
        }
        res.send(theCltAccts);
    } catch (error){
        if (error instanceof MissingResourceError){
            res.send(error);
            res.status(404);
        }
    }
});


// GET /accounts?amountLessThan=2000&amountGreaterThan400 => get all accounts for between 400 and 2000
app.get("/accounts", async (req, res) => {
    try {

        let amtAccts = [];
        const lessThan = Number(req.query.amountLessThan);
        const greaterThan = Number(req.query.amountGreaterThan);

        console.log(lessThan);
        console.log(greaterThan);

        let accts:Account[] = await accountService.retrieveAllAccounts();
        for (let i = 0; i < accts.length; i++){
            if (accts[i].amount > greaterThan && accts[i].amount <= lessThan){
                amtAccts.push(accts[i]);
            }
        }
    res.send(amtAccts);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.send(error);
            res.status(404);
        }
    }
});

// GET /accounts/4 => get account with id 4 
// 	return 404 if no account or client exists
app.get("/accounts/:id", async (req, res) => {
    try {
        const accountId = Number(req.params.id);
        const account:Account = await accountService.retrieveAccountById(accountId);
        res.send(account);
    } catch (error){
        if (error instanceof MissingResourceError){
            res.send(error);
            res.status(404);
        }
    }
    
});


// PUT /accounts/3 => update account with the id 3
// 	return 404 if no account or client exists
app.put("/accounts/:id", async (req, res)=>{
    try {
        const accountId = Number(req.params.id);
        const acct:Account = await accountService.retrieveAccountById(accountId);
        const newAcctType:string = req.body.type;
        await accountService.updateAccount(acct, newAcctType);
        res.send(acct);
    } catch (error){
        if (MissingResourceError){
            res.send(error);
            res.status(404);
        }
    }
    
});

// DELETE /accounts/6 => delete account 6 
// 	return 404 if no account or client exists
app.delete("/accounts/:id", async (req, res)=>{
    try {
        const accountId = Number(req.params.id);
        await accountService.removeAccountById(accountId);
        res.send(`${accountId} has been deleted.`);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(MissingResourceError);
        }
    }
});

// add types first
// makes clearer what types I have as opposed to waiting on a Promise

// PATCH /accounts/12/deposit => deposit given amount (Body {"amount":500} )
// 	return 404 if no account exists
app.patch("/accounts/:id/deposit", async (req, res)=>{
    try {
        const accountId = Number(req.params.id);
        const amount = Number(req.body.amount);
        let acct:Account = await accountService.retrieveAccountById(accountId);
        acct = await accountService.depositMoney(acct, amount);
        res.send(acct);
    } catch (error){
        if (error instanceof MissingResourceError){
            res.send(error)
            res.status(404);
        }
    }
});

// PATCH /accounts/12/withdraw => withdraw given amount (Body {"amount":500} )
// 	return 404 if no account exists
// 	return 422 if insufficient funds
app.patch("/accounts/:id/withdraw", async (req, res)=>{
    const accountId = Number(req.params.id);
    const amount = req.body.amount;
    let acct:Account = await accountService.retrieveAccountById(accountId);
    try {
        acct = await accountService.withdrawMoney(acct, amount);
        res.send(acct);
        // if (acct.amount < amount){
        //     console.log(acct.amount);
        //     res.status(422);
        //     res.send("Insufficient funds");
        // }
    } catch(error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        } else if (OverdrawnError){
            if (acct.amount < amount){
                console.log(acct.amount);
                res.status(422);
                res.send("Insufficient funds");
            }
        }
    }
});


app.listen(3000, ()=>{console.log("Application Started!")})