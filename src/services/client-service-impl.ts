import { MissingResourceError } from "../../bank-app-repo/src/errors";
import { ClientDAO } from "../DAOs/client-dao";
import { ClientDaoPostgres } from "../DAOs/client-dao-postgres";
import { Client } from "../entities";
import ClientService from "./client-service";

export class ClientServiceImpl implements ClientService{
    
    clientDao:ClientDAO = new ClientDaoPostgres()

    registerClient(client: Client): Promise<Client> {
       return this.clientDao.createClient(client);
    }

    retrieveAllClients(): Promise<Client[]> {
        return this.clientDao.getAllClients();
    }

    retrieveClientById(clientId: number): Promise<Client> {
        return this.clientDao.getClientById(clientId);
    }

    updateClient(client: Client): Promise<Client> {
        return this.clientDao.updateClient(client);
    }

    removeClientById(clientId: number): Promise<boolean> {
        if (this.clientDao.getClientById(clientId) === null){
            throw new MissingResourceError("Client not found.");
        }
        return this.clientDao.deleteClientById(clientId);
    }

}