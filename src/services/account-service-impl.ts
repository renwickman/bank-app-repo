import { AccountDAO } from "../DAOs/account-dao"
import { Account, Client } from "../entities";
import AccountService from "./account-service";
import { AccountDaoPostgres } from "../DAOs/account-dao-postgres";
import { OverdrawnError } from "../../bank-app-repo/src/errors";
//methods: Withdraw, Deposit, Overdrawn

const testClient:Client = new Client(0, 'Oswald', 'Cobblepot');

export class AccountServiceImpl implements AccountService{
   
    
    accountDao:AccountDAO = new AccountDaoPostgres();
    

    registerAccount(account: Account, clientId: number): Promise<Account> {
        account.acctNumber = Math.floor(Math.random() * 900000000) + 100000000;
        account.amount = 50;
        account.type = "Checking";
        account.clientId = clientId;
        return this.accountDao.createAccount(account);
    }

    retrieveAllAccounts(): Promise<Account[]> {
        return this.accountDao.getAllAccounts();
    }

    withdrawMoney(account: Account, minus: number): Promise<Account> {
        account.amount = account.amount - minus;
        if (account.amount < 0){
            //account.amount = account.amount + minus;
            throw new OverdrawnError("Insufficient Funds");
        }
        return this.accountDao.updateAccount(account);
    }

    depositMoney(account: Account, add: number): Promise<Account> {
        account.amount = account.amount + add;
        return this.accountDao.updateAccount(account);
    }

    retrieveAccountById(accountId: number): Promise<Account> {
        return this.accountDao.getAccountById(accountId);
    }

    updateAccount(account: Account, accountType: string): Promise<Account> {
        account.type = accountType;
        return this.accountDao.updateAccount(account);
    }

    removeAccountById(accountId: number): Promise<boolean> {
        return this.accountDao.deleteAccountById(accountId);
    }
    
}