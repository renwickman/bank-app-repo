import { Account } from '../entities';


export default interface AccountService{

    registerAccount(account:Account, clientId: number):Promise<Account>;

    retrieveAllAccounts():Promise<Account[]>

    withdrawMoney(account:Account, amount:number):Promise<Account>;

    depositMoney(account:Account, amount:number):Promise<Account>;

    retrieveAccountById(accountId:number):Promise<Account>;

    updateAccount(account:Account, accountType:string):Promise<Account>;

    removeAccountById(accountId:number):Promise<boolean>;
    
}