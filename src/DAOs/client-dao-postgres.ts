import { Client } from '../entities';
import { ClientDAO } from './client-dao';
import { client } from "../connection";
import { MissingResourceError } from '../errors';

export class ClientDaoPostgres implements ClientDAO {

    async createClient(newClient: Client): Promise<Client> {
        const sql:string = "insert into client(first_name, last_name) values ($1,$2) returning client_id"
        const values = [newClient.fname, newClient.lname];
        const result = await client.query(sql, values);
        newClient.clientId = result.rows[0].client_id;
        console.log(newClient.clientId);
        return newClient;
    }

    async getClientById(clientId: number): Promise<Client> {
        const sql:string = "select * from client where client_id = $1";
        const values = [clientId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${clientId} does not exist!`);
        }
        const row = result.rows[0];
        const theClient:Client = new Client(
                row.client_id,
                row.first_name,
                row.last_name);
            return theClient;
        }


    async getAllClients(): Promise<Client[]> {
        const sql:string = "select * from client";
        const result = await client.query(sql);
        const clients:Client[] = [];
        for (const row of result.rows){
            const newClient:Client = new Client(
                row.client_id,
                row.first_name,
                row.last_name);
            clients.push(newClient);
        }
        return clients;
    }

   
    async updateClient(updateClient: Client): Promise<Client> {
        //sql query
        const sql:string = 'update client set first_name=$1, last_name=$2 where client_id=$3'
        //need to have values to replace the '$' signs
        const values = [updateClient.fname, updateClient.lname, updateClient.clientId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${updateClient.clientId} does not exist`);
        }
        return updateClient;
    }

    async deleteClientById(clientId: number): Promise<boolean> {
        const sql:string = 'delete from client where client_id = $1'
        const values = [clientId];
        const result = await client.query(sql, values);
        console.log(result);
        console.log(result.rowCount);
        if (result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${clientId} does not exist`)
        }
        return true;
    }
    
}