import { Account } from "../entities";
import { AccountDAO } from "./account-dao";
import { client } from "../connection";
import { MissingResourceError } from '../errors';

export class AccountDaoPostgres implements AccountDAO {
    
    async createAccount(account: Account): Promise<Account> {
        console.log(account);
        const sql:string = "insert into account(acct_number, amount, acct_type, c_id) values ($1,$2,$3,$4) returning account_id"
        const values = [account.acctNumber, account.amount, account.type, account.clientId];
        const result = await client.query(sql, values);
        account.accountId = result.rows[0].account_id;
        return account;
    }

    async getAccountById(accountId: number): Promise<Account> {
        const sql:string = "select * from account where account_id = $1";
        const values = [accountId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        const row = result.rows[0];
        // returns the specific object
        const account:Account = new Account(
            row.account_id,
            Number(row.acct_number),
            row.amount,
            row.acct_type,
            row.c_id
            );
        console.log(account);
        return account;
    }

    async getAllAccounts(): Promise<Account[]> {
        const sql:string = "select * from account";
        const result = await client.query(sql);
        const accounts:Account[] = [];
        for (const row of result.rows){
            const newAccount:Account = new Account(
                row.account_id,
                row.acct_number,
                row.amount,
                row.acct_type,
                row.c_id);
            accounts.push(newAccount);
        }
        return accounts;
    }

    async updateAccount(updateAccount: Account): Promise<Account> {
        //sql query
        const sql:string = 'update account set acct_number=$1, amount=$2, acct_type=$3, c_id=$4 where account_id=$5'
        //need to have values to replace the '$' signs
        const values = [updateAccount.acctNumber, updateAccount.amount, updateAccount.type, updateAccount.clientId, updateAccount.accountId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${updateAccount.accountId} does not exist`);
        }
        return updateAccount;
    }

    async deleteAccountById(accountId: number): Promise<boolean> {
        const sql:string = 'delete from account where account_id = $1'
        const values = [accountId];
        const result = await client.query(sql, values);
        if (result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`)
        }
        return true;
    }

}