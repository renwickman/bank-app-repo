// Just CRUD operations
import { Account } from '../entities';


export interface AccountDAO{

    // CREATE
    createAccount(account:Account):Promise<Account>;

    // READ
    getAccountById(accountId:number):Promise<Account>;
    getAllAccounts():Promise<Account[]>;

    // PUT
    updateAccount(account:Account):Promise<Account>;

    // DELETE
    deleteAccountById(accountId:number): Promise<boolean>;
}