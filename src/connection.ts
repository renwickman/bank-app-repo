import { Client } from 'pg';
require('dotenv').config({path:'C:\\Users\\renwi\\Desktop\\Revature_Learning\\BankApp\\app.env'});

export const client = new Client({
    user: 'postgres',
    password: process.env.DBPASSWORD,
    database: process.env.DATABASENAME,
    port:5432,
    host: '34.69.127.190'
})
client.connect()