export class Client{
    constructor(
        public clientId:number,
        public fname:string,
        public lname:string,
    ){}
}

export class Account{
    constructor(
        public accountId:number,
        public acctNumber:number,
        public amount:number,
        public type:string = "Checking",
        public clientId:number
    ){}
}